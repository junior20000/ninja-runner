# Ninja Runner
> Uninove Integrated Project 2021 (Digital Games)

***Ninja Runner*** is a competitive endless runner with online leaderboard

## Project Requirements

- Engine: Godot
- Platform: Windows and WEB
- 2D Only
- Online Leaderboard
- Leaderboard must be in PHP
- Database must be MariaDB
- XAMPP

## Built With
 * [Godot](https://godotengine.org/)

## Versioning
I use [GIT](https://git-scm.com/) for versioning.

## Author
 * [Roberto Schiavelli Júnior](https://www.linkedin.com/in/roberto-schiavelli-j%C3%BAnior-86a3561a9/) - March 2021

## Copyright
© Roberto Schiavelli Júnior - All Rights Reserved  
Unauthorized copying of this project and any files within it, via any medium is strictly prohibited  
Proprietary and confidential
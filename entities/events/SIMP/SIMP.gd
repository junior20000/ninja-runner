extends Node2D

var target: Node2D
var spawn_pos: Vector2
var el_time: float = 1
var t = 0

var sfx := preload("res://sfx/audio/SIMP.ogg")

#var topLeft = get_parent().get_camera_screen_center() - get_viewport_rect().size / 2

func _ready():
	$Warning/TrackTimer.connect("timeout", self, "_on_target_locked")
	$Shot/ASP.stream = sfx
	
func _process(delta: float):
	track_target(delta)
	el_time += delta
	$Warning/AnimationPlayer.playback_speed = el_time
	
func start():
	$Warning/TrackTimer.start()
	global_position.y = target.global_position.y - 10
	$Warning.visible = true
	$Warning/AnimationPlayer.play("Blink")
	set_process(true)

func is_valid_target():
	if target == null:
		get_node("TriggerTimer").stop()
		set_process(false)
		return false
	return true

func track_target(delta):
	if not is_valid_target():
		return
	t += delta * 0.1
	var inter = global_position.y + ((target.global_position.y - 10) - global_position.y) * t
	global_position.y = inter 

func _on_target_locked():
	set_process(false)
	$Warning.visible = false
	el_time = 1
	t = 0
	$Shot.start()

func _on_event_triggered():
	start()

extends CanvasLayer

func _ready():
	$ASP.connect("finished", self, "_on_audio_finished")
	$Tween.interpolate_property($Sprite, "modulate", 
	  Color(1, 1, 1, 0), Color(1, 1, 1, 1), 1.5, 
	  Tween.TRANS_LINEAR, Tween.EASE_IN)
	$ASP.play()
	$Tween.start()

func _on_audio_finished():
	$Tween.interpolate_property($Sprite, "modulate", 
	  Color(1, 1, 1, 1), Color(1, 1, 1, 0), 1.5, 
	  Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.start()
	$Tween.connect("tween_all_completed", self, "_on_badge_finished")
	
func _on_badge_finished():
	queue_free()

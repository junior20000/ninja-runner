extends RayCast2D

var ent_name = "Sniper Monkey"

func _ready():
	#position.y = 300
	set_physics_process(false)
	$Line2D.points[1] = Vector2.ZERO
	
	$Cooldown.connect("timeout", self, "_on_cooldown_timeout")
	$AliveTime.connect("timeout", self, "_on_alivetime_timeout")

func _physics_process(delta):
	var collision_cord = cast_to
	force_raycast_update()
	
	$CollisionParticles.emitting = is_colliding()
	
	if is_colliding():
		collision_cord = to_local(get_collision_point())
		$CollisionParticles.global_rotation = get_collision_normal().angle()
		$CollisionParticles.position = collision_cord
		get_collider().hit(ent_name)

	$Line2D.points[1] = collision_cord
	$ShotTrailParticles.position = collision_cord * 0.5
	$ShotTrailParticles.process_material.emission_box_extents.x = collision_cord.length() * 0.5

func _on_cooldown_timeout():
	set_physics_process(true)
	$GunParticles.emitting = true
	$ShotTrailParticles.emitting = true
	charge()
	$ASP.play()
	$AliveTime.start()

func _on_alivetime_timeout():
	$GunParticles.emitting = false
	$ShotTrailParticles.emitting = false
	$CollisionParticles.emitting = false
	discharge()
	set_physics_process(false)

func start():
	$Cooldown.start()

func charge():
	$Tween.stop_all()
	$Tween.interpolate_property($Line2D, "width", 0, 10, 0.2)
	$Tween.start()
	
func discharge():
	$Tween.stop_all()
	$Tween.interpolate_property($Line2D, "width", 10, 0, 0.2)
	$Tween.start()

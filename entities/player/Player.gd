extends KinematicBody2D

signal player_died(motive)

var health: int = 1
var motion: Vector2 = Vector2()

onready var exp_prt = preload("res://vfx/SpriteExplosion/SpriteExplosion.tscn")
onready var simp_kb = preload("res://entities/events/SIMP/kill_badge/kill_badge.tscn")

func _ready():
	$AnimationTree.active = true

func hit(ent_name: String):
	health -= 1
	
	if health <= 0:
		die(ent_name)

func die(ent_name: String):
	var ex = exp_prt.instance()
	
	ex.init($Pivot/Body, self.scale)
	ex.position = self.global_position

	get_tree().get_root().add_child(ex)
	
	GameInfo.death_by = ent_name
	GameInfo.distance = position.x
	emit_signal("player_died", ent_name)
	
	if ent_name == "Sniper Monkey":
		var spkb = simp_kb.instance()
		get_tree().get_root().add_child(spkb)
	
	#get_parent().get_node("Camera/SIMP").queue_free()
	queue_free()

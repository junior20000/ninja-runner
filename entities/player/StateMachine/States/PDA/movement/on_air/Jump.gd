extends PDAState

export (float) var gravity = 450
export (float) var jump_force = 170
export (float) var max_jump = 300

onready var asp = get_parent().get_parent().get_node("ASP")
var at: AnimationTree

func _ready():
	is_pushable = true

func _enter(player: KinematicBody2D):
	asp.play()
	player.motion.y = -jump_force
	at = player.get_node("AnimationTree")
	at.get("parameters/playback").travel("Jump")


func _exit(player: KinematicBody2D):
	pass

func input(player: KinematicBody2D, event: InputEvent):
	pass

func update(player: KinematicBody2D, delta: float):
	player.motion.y += gravity * delta
	
	if player.motion.y > 200:
		player.motion.y = 200

	if player.motion.y > 0:
		at.get("parameters/playback").travel("Fall")
	
	player.motion = player.move_and_slide(player.motion, Vector2(0, -1))

	if player.is_on_floor():
		return "return"

extends "on_ground.gd"

export (float) var INITIALSPEED = 4000
export (float) var gravity = 450  #350
var speed: int

var velocity: Vector2 = Vector2()

var at: AnimationTree

func _ready():
	is_pushable = true
	speed = INITIALSPEED

func _enter(player: KinematicBody2D):
	duck(player)
	
	at = player.get_node("AnimationTree")
	at.get("parameters/playback").travel("Crouch")

func _exit(player: KinematicBody2D):
	stand(player)

func input(player: KinematicBody2D, event: InputEvent):
	pass

func update(player: KinematicBody2D, delta: float):
	
	var nrm_speed = speed * delta
	player.motion.x = nrm_speed
	player.motion.y += gravity * delta
	
	player.motion = player.move_and_slide(player.motion, Vector2(0, -1))
	
	if not Input.is_action_pressed("crouch"):
		return "Move"


func duck(player):
	player.get_node("Collision").disabled = true
	player.get_node("Hitbox/Collision").disabled = true
	player.get_node("Hitbox/CollisionCrouch").disabled = false
	player.get_node("CollisionCrouch").disabled = false
	
func stand(player):
	player.get_node("Hitbox/Collision").disabled = false
	player.get_node("Collision").disabled = false
	player.get_node("Hitbox/CollisionCrouch").disabled = true
	player.get_node("CollisionCrouch").disabled = true

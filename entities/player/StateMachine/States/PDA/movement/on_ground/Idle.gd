extends "on_ground.gd"

var at: AnimationTree

func _ready():
	is_pushable = false

func _enter(player: KinematicBody2D):
	at = player.get_node("AnimationTree")
	at.get("parameters/playback").travel("Idle")

func _exit(player: KinematicBody2D):
	pass 

func input(player: KinematicBody2D, event: InputEvent):
	return .input(player, event)

func update(player: KinematicBody2D, delta: float):
	if player.motion.x > 0:
		print(player.motion.x)
		return "Move"

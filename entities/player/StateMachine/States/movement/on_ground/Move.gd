extends "on_ground.gd"

export (float) var INITIALSPEED = 8000
export (float) var gravity = 350
var speed: int

var velocity: Vector2 = Vector2()

var at: AnimationTree

func _ready():
	speed = INITIALSPEED

func _enter(player: KinematicBody2D):
	at = player.get_node("AnimationTree")
	at.get("parameters/playback").travel("Move")

func _exit(player: KinematicBody2D):
	pass

func input(player: KinematicBody2D, event: InputEvent):
	return .input(player, event)

func update(player: KinematicBody2D, delta: float):
	var nrm_speed = speed * delta
	player.motion.x = nrm_speed
	player.motion.y += gravity * delta
	
	player.motion = player.move_and_slide(player.motion, Vector2(0, -1))

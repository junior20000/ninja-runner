extends Label


func _ready():
	get_parent().get_parent().get_node("StateMachine").connect("state_changed", self, "_on_Player_state_changed")

func _on_Player_state_changed(new_state_name):
	text = new_state_name

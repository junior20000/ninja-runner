extends CanvasLayer

onready var actor: Node2D = null

func _ready():
	$TopLeftCT/UsernameLB.text = GameInfo.username
	ScoreManager.connect("score_updated", self, "_on_score_update")
	$TopRightCT/ScoreContainer/ScoreNumLB.text = "0"
	

func _on_score_update(score: int):
	$TopRightCT/ScoreContainer/ScoreNumLB.text = str(score)

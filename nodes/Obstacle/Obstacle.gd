extends RigidBody2D
class_name Obstacle

signal body_collided
signal body_highspeed_collided

var ent_name: String
var health: int

var jump_score: int
var crouch_score: int
var hit_score: int
var destruction_score: int

func _ready():
	pass

func _on_collision():
	pass
	
func _on_high_speed_collision():
	pass
	
func _on_sword_hit():
	pass

func _spawn_config():
	pass

extends Node
class_name PushdownAutomaton

signal state_changed(state_name)
signal stack_updated(stack)
#signal state_set(state_name)

var host: Node = null
var state: PDAState = null setget set_state
var stack: Array = []
onready var states: Dictionary = {}

### GETTERS & SETTERS ###

func set_state(new_state: PDAState):
	state = new_state
	stack.push_front(state)
	_travel(state.get_name())

### GETTERS & SETTERS END ###

func _physics_process(delta: float):
	var new_state = state.update(host, delta)
	if new_state:
		_travel(new_state)

func _input(event):
	var new_state = state.input(host, event)
	if new_state:
		_travel(state.input(host, event))

func _travel(state_name: String) -> void:
	state._exit(host)
	
	if state_name == "return":
		stack.pop_front()
	elif state_name in ["Jump"]:
		stack.push_front(states[state_name])
	else:
		stack[0] = states[state_name]
	
	state = stack[0]
	
	state._enter(host)
	
	emit_signal("state_changed", state.get_name())
	emit_signal("stack_updated", stack)

func populate() -> void:
	for child in self.get_children():
		states[child.get_name()] = child

func populate_from_node(node: Node) -> void:
	for child in node.get_children():
		states[child.get_name()] = child

extends Node
class_name State

"""
State initializer (on enter)
"""
func _enter(controller):
	pass

"""
State destroyer (on exit)
"""
func _exit(controller):
	pass

func input(controller, event: InputEvent):
	pass

func update(controller, delta: float):
	pass

"""
Gets player input direction.
returns -1 if left, 1 if right.
"""
func get_input_direction() -> Vector2:  # Change name. Too long
	var direction = Vector2()
	direction.x = int(Input.is_action_pressed("right")) - int(Input.is_action_pressed("left"))
	
	return direction

extends Node
class_name StateMachine

signal state_changed(state_name)
#signal state_set(state_name)

var host: Node = null
var state: State = null setget set_state
onready var states: Dictionary = {}

### GETTERS & SETTERS ###

func set_state(new_state: State):
	state = new_state
	_travel(state.get_name())
	#emit_signal("state_set", state.get_name())

### GETTERS & SETTERS END ###

func _physics_process(delta: float):
	var new_state = state.update(host, delta)
	if new_state:
		_travel(new_state)

func _input(event):
	var new_state = state.input(host, event)
	if new_state:
		_travel(state.input(host, event))

func _travel(state_name: String) -> void:
	state._exit(host)
	state = states[state_name]
	state._enter(host)
	
	emit_signal("state_changed", state.get_name())

func populate() -> void:
	for child in self.get_children():
		states[child.get_name()] = child

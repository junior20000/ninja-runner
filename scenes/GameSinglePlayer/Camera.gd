extends Camera2D

signal target_changed(target)

onready var target_node: Node2D = null

func _ready():
	pass

func _physics_process(delta):
	follow_target(target_node)

func follow_target(target: Node2D):
	if target == null:
		set_physics_process(false)
		return
	
	position.x = target.position.x

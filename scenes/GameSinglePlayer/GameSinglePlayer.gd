extends Node2D

onready var game_over_scn = preload("res://ui/aftermatch/Aftermatch.tscn")

func _ready():
	GameInfo.gamemode = GameInfo.GameMode.OFFLINE_SINGLEPLAYER
	$Player.connect("player_died", self, "on_game_over")
	$Camera.target_node = $Player
	$Camera/SIMP.target = $Player
	#$Camera/SIMP.start()

func on_game_over(killer):
	$PlayerDeath.play()
	var go = game_over_scn.instance()
	go.connect("restart_requested", self, "on_restart_pressed")
	go.connect("menu_requested", self, "on_menu_pressed")
	Gateway.send_score()
	get_node("UI").add_child(go)

func on_menu_pressed():
	reset_game_data()
	get_tree().change_scene("res://scenes/TitleScreen/TitleScreen.tscn")

func on_restart_pressed():
	reset_game_data()
	get_tree().reload_current_scene()

func reset_game_data():
	ScoreManager.flush_session_data()
	GameInfo.flush_session_data()
	Engine.time_scale = GameInfo.diff

func _exit_tree():
	queue_free()

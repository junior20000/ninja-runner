extends Timer

func _ready():
	connect("timeout", self, "on_timeout")
	get_parent().get_parent().get_parent().get_node("Player").connect("player_died", self, "on_player_death")

func on_player_death(killer):
	stop()

func on_timeout():
	var parent = get_parent()
	parent.start()

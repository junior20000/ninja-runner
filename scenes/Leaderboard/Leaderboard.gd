extends Control

onready var lbc_scn = preload("res://scenes/Leaderboard/LBCell.tscn")
onready var hr = get_node("HTTPRequest")
onready var cls_btn = get_node("Background/CloseBtn")
onready var asp = get_node("ASP")

var req_path = "http://crawfish.ddns.net:2424/api/get_leaderboard.php"

func _ready():
	cls_btn.connect("pressed", self, "on_btn_pressed")
	cls_btn.connect("pressed", self, "on_close_pressed")
	hr.connect("request_completed", self, "on_request_completed")
	hr.request(req_path)

func on_btn_pressed():
	asp.play()

func on_close_pressed():
	get_tree().change_scene("res://scenes/TitleScreen/TitleScreen.tscn")

func on_request_completed(result, response_code, headers, body):
	var response = parse_json(body.get_string_from_utf8())
	for resp in response:
		var lbc = lbc_scn.instance()
		lbc.get_node("UserLB").text = resp["username"]
		lbc.get_node("ScoreLB").text = resp["score"]
		get_node("Background/VBoxContainer/ScrollContainer/VBoxContainer").add_child(lbc)

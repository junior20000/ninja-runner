extends Control

onready var game_scn = preload("res://scenes/GameSinglePlayer/GameSinglePlayer.tscn")
onready var opt_ui = preload("res://ui/options/Options.tscn")
onready var start_btn = get_node("Background/VBoxContainer/BtnCtn/StartBtn")
onready var lb_btn = get_node("Background/VBoxContainer/BtnCtn/LeaderboardBtn")
onready var opt_btn = get_node("Background/VBoxContainer/BtnCtn/OptionsBtn")
onready var asp = get_node("ASP")


func _ready():
	$AP.play("Idle")
	start_btn.connect("pressed", self, "on_btn_pressed")
	opt_btn.connect("pressed", self, "on_btn_pressed")
	lb_btn.connect("pressed", self, "on_btn_pressed")
	
	start_btn.connect("pressed", self, "on_start_pressed")
	opt_btn.connect("pressed", self, "on_opt_pressed")
	lb_btn.connect("pressed", self, "on_lb_pressed")
	

func on_btn_pressed():
	asp.play()

func on_start_pressed():
	get_tree().change_scene("res://scenes/GameSinglePlayer/GameSinglePlayer.tscn")

func on_lb_pressed():
	get_tree().change_scene("res://scenes/Leaderboard/Leaderboard.tscn")

func on_opt_pressed():
	var opt = opt_ui.instance()
	add_child(opt)

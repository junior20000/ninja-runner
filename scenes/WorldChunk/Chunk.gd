extends Node2D

signal chunk_spawned
signal chunk_destroyed

onready var vn = $VisibilityNotifier2D
onready var scn_rock = preload("./Obstacles/Rock/Rock.tscn")
onready var scn_bird = preload("./Obstacles/Bird/Bird.tscn")
onready var obs_map = [scn_rock, scn_bird]

const CHUNK_SIZE = 400

var spawn_points = [80, 160, 240, 320]
var has_obstacles = true  # Setget

func _ready():
	$DifficultyTrigger/Collision.disabled = false
	
	vn.connect("screen_exited", self, "_on_screen_exit")
	
	if has_obstacles:
		fill_obstacles()
		
	emit_signal("chunk_spawned")

func _on_screen_exit():
	emit_signal("chunk_destroyed")
	queue_free()

func fit_on_chunk(obj_size, space_left) -> bool:
	return false

func spawn_obstacle(obs: Obstacle, pos_x: int) -> void:
	obs._spawn_config()
	obs.position.x = pos_x
	$Obstacles.add_child(obs)

func fill_obstacles():
	randomize()
	
	for sp in spawn_points:
		var obs = obs_map[randi() % obs_map.size()].instance()
		spawn_obstacle(obs, sp)
		#rock.scale = Vector2(0.5, 0.5)
		#rock.position = Vector2(sp, -64)
		#$Obstacles.add_child(rock)

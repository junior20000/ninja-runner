extends Node

const scn_chunk = preload("res://scenes/WorldChunk/Chunk.tscn")
const chunk_width = 400
const chunk_pool_size = 2

func _ready():
	spawn_and_move(false)
	for f in range(chunk_pool_size):
		spawn_and_move()

func spawn(has_obstacles) -> void:
	var chunk = scn_chunk.instance()
	chunk.has_obstacles = has_obstacles
	chunk.position = self.position
	#print(self.position)
	chunk.get_node("VisibilityNotifier2D").connect("screen_exited", self, "_on_chunk_screen_exit")
	$Container.call_deferred("add_child", chunk)

func move_next():
	self.position.x += chunk_width
	
func spawn_and_move(has_obstacles = true):
	spawn(has_obstacles)
	move_next()

func _on_chunk_screen_exit():
	spawn_and_move()

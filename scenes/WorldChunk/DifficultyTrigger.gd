extends Area2D

signal difficulty_updated(difficuly)

var rng = RandomNumberGenerator.new()

func _ready():
	rng.randomize()
	connect("body_entered", self, "_on_body_entered")

func _on_body_entered(body: Node):
	GameInfo.diff = rng.randf_range(1, 1.5)
	Engine.set_time_scale(GameInfo.diff)
	emit_signal("difficulty_updated", GameInfo.diff)

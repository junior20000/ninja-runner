extends Obstacle

var heights = [-70, -85]

func _ready():
	position.y = heights[randi() % heights.size()]
	
	ent_name = "Bird"
	health = 1
	
	jump_score = 3
	crouch_score = 2
	hit_score = 1
	destruction_score = 5
	
	$Hitbox.connect("body_entered", self, "_on_body_entered")
	$JumpScoreArea.connect("body_exited", self, "_on_osbtacle_jumped")
	$CrouchScoreArea.connect("body_exited", self, "_on_osbtacle_crouched")
	
	$AnimationPlayer.play("Fly")

func _on_collision():
	pass
	
func _on_high_speed_collision():
	pass
	
func _on_sword_hit():
	pass


func _on_body_entered(body: Node):
	if body.has_method("hit"):
		body.hit(ent_name)

func _on_osbtacle_jumped(body: Node):
	ScoreManager.add_points(jump_score)
	
func _on_osbtacle_crouched(body: Node):
	ScoreManager.add_points(crouch_score)
	


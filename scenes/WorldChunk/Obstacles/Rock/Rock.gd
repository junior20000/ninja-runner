extends Obstacle

func _ready():
	position.y = -64
	
	ent_name = "Rock"
	health = 3
	jump_score = 2
	crouch_score = 0
	hit_score = 1
	destruction_score = 5
	
	$Hitbox.connect("body_entered", self, "_on_body_entered")
	$ScoreArea.connect("body_exited", self, "_on_osbtacle_jumped")

func _on_collision():
	pass
	
func _on_high_speed_collision():
	pass
	
func _on_sword_hit():
	pass


func _on_body_entered(body: Node):
	if body.has_method("hit"):
		body.hit(ent_name)

func _on_osbtacle_jumped(body: Node):
	ScoreManager.add_points(jump_score)

func _spawn_config():
	scale = Vector2(0.5, 0.5)

extends Node

var file_path = OS.get_executable_path().get_base_dir() + "\\config\\config.ini"
var dir_path = OS.get_executable_path().get_base_dir() + "\\config"

var resolutions = {
	"1920x1080": 0,
	"1600x900": 1,
	"1366x768": 2,
	"1280x720": 3,
	"960x540": 4,
	"854x480": 5,
	"848x480": 6,
	"640x360": 7
}
var window_modes = [0, 1, 2]
var resolution = "640x360" setget set_resolution
var window_mode = 0 setget set_window_mode

var sd_master: int = 0
var sd_bgm: int = 0
var sd_sfx: int = 0
var sd_ui: int = 0

func _ready():
	#var config = ConfigFile.new()
	load_config()
	
func set_resolution(new_res):
	resolution = new_res
	var wh = res2vect2()
	OS.set_window_size(wh)
	OS.set_window_position(OS.get_screen_size()*0.5 - wh*0.5)
	print(OS.get_window_size())

func set_window_mode(wm):
	if wm == 0:
		OS.window_fullscreen = false
		OS.set_window_position(OS.get_screen_size()*0.5 - res2vect2()*0.5)
	elif wm == 1:
		resolution = str(OS.get_screen_size().x) + "x" + str(OS.get_screen_size().y)
		OS.window_fullscreen = true
	
	window_mode = wm

func res2vect2():
	var wh = resolution.split("x")
	return Vector2(wh[0], wh[1])

func build_dir():
	var dir = Directory.new()
	
	if not dir.dir_exists(dir_path):
		dir.open(OS.get_executable_path().get_base_dir())
		dir.make_dir("config")
		
func generate_config_file():
	var config = ConfigFile.new()
	#config.secti
	
func load_config():
	var config = ConfigFile.new()
	print(file_path)
	if config.load(file_path) == OK:
		for sec in config.get_sections():
			for key in config.get_section_keys(sec):
				var value = config.get_value(sec, key)

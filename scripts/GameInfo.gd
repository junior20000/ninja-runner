extends Node

enum GameMode {OFFLINE_SINGLEPLAYER, ONLINE_SINGLEPLAYER, ONLINE_MULTIPLAYER}

var username: String
var gamemode: int

var diff: float = 1

var death_by: String

var distance: int = 0
var rocks_jumped: int = 0
var birds_jumped: int = 0
var birds_ducked: int = 0

func flush_session_data():
	diff = 1
	death_by = ""
	distance = 0
	rocks_jumped = 0
	birds_jumped = 0
	birds_ducked = 0

extends Node

signal score_updated(score)

var score: int setget set_score
var final_score: int = 0 setget , get_final_score

func _ready():
	score = 0

func flush_session_data():
	score = 0
	final_score = 0

func get_final_score():
	final_score = (GameInfo.distance * 0.3) + score
	return final_score

func set_score(new_score: int):
	score = new_score
	emit_signal("score_updated", score)

func add_points(points: int):
	score += points
	emit_signal("score_updated", score)

extends Node

signal login_request_sent(username)
signal login_failed()

var network = NetworkedMultiplayerENet.new()
var gtwy_mpapi = MultiplayerAPI.new()
var ip = "crawfish.ddns.net"
var port = 6969

var username: String
var password: String

func _ready():
	network.connect("connection_failed", self, "on_connection_failed")
	network.connect("connection_succeeded", self, "on_connection_succeeded")
	start_connection()

func _process(delta):
	if get_custom_multiplayer() == null:
		return
	if not custom_multiplayer.has_network_peer():
		return
	
	custom_multiplayer.poll()

func start_connection():
	network.create_client(ip, port)
	set_custom_multiplayer(gtwy_mpapi)
	custom_multiplayer.set_root_node(self)
	custom_multiplayer.set_network_peer(network)

func request_login(usr: String, pswrd: String):
	username = usr
	password = pswrd
	rpc_id(1, "login_request", username, password)
	clear_credentials()

func send_score():
	rpc_id(1, "send_score", GameInfo.username, ScoreManager.final_score, GameInfo.death_by)

func clear_credentials():
	username = ""
	password = ""

remote func return_login_request(response_code, response_msg, usr):
	if int(response_code) == 0:
		GameInfo.username = usr
		get_tree().change_scene("res://scenes/TitleScreen/TitleScreen.tscn")
	else:
		emit_signal("login_failed")
	print(str(response_code) + " " + response_msg)

func on_login_request_sent():
	pass

func on_connection_succeeded():
	print("Gateway connected")

func on_connection_failed():
	print("Failed to connect to the gateway")

extends Control

onready var usr_input = get_node("Background/VBoxContainer/UsernameField")
onready var pswrd_input = get_node("Background/VBoxContainer/PasswordField")
onready var login_btn = get_node("Background/VBoxContainer/HBoxContainer/LoginBtn")
onready var register_btn = get_node("Background/VBoxContainer/HBoxContainer/RegisterBtn")
onready var asp = get_node("ASP")

func _ready():
	Gateway.connect("login_failed", self, "on_login_fail")
	
	login_btn.connect("pressed", self, "on_login_pressed")
	login_btn.connect("pressed", self, "on_btn_pressed")
	register_btn.connect("pressed", self, "on_btn_pressed")
	
func on_login_pressed():
	if not validate_username():
		return
	if not validate_password():
		return
	
	login_btn.disabled = true
	Gateway.request_login(usr_input.text, pswrd_input.text)

func on_login_fail():
	login_btn.disabled = false

func on_btn_pressed():
	asp.play()

func validate_username():
	if usr_input.text == "":
		print("Blank username")
		return false
	return true

func validate_password():
	if pswrd_input.text == "":
		print("Blank password")
		return false
	return true

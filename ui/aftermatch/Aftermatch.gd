extends Control

signal menu_requested()
signal restart_requested()

onready var menu_btn = get_node("Background/VBoxContainer/ButtonContainer/MenuBtn")
onready var restart_btn = get_node("Background/VBoxContainer/ButtonContainer/RestartBtn")
onready var death_by = get_node("Background/VBoxContainer/ScrollContainer/VBoxContainer/KillerLb")
onready var distance_lb = get_node("Background/VBoxContainer/ScrollContainer/VBoxContainer/DistanceContainer/ValueLb")
onready var score_lb = get_node("Background/VBoxContainer/ScrollContainer/VBoxContainer/ScoreContainer/ValueLb")
onready var final_score_lb = get_node("Background/VBoxContainer/ScrollContainer/VBoxContainer/FinalScoreContainer/ValueLb")
onready var asp = get_node("ASP")

func _ready():
	menu_btn.connect("pressed", self, "on_btn_pressed")
	menu_btn.connect("pressed", self, "on_menu_pressed")
	restart_btn.connect("pressed", self, "on_btn_pressed")
	restart_btn.connect("pressed", self, "on_restart_pressed")
	
	death_by.text = GameInfo.death_by
	distance_lb.text = str(GameInfo.distance)
	score_lb.text = str(ScoreManager.score)
	final_score_lb.text = str(ScoreManager.final_score)

func on_menu_pressed():
	emit_signal("menu_requested")

func on_restart_pressed():
	emit_signal("restart_requested")

func on_btn_pressed():
	asp.play()

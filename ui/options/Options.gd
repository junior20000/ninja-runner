extends Control

onready var close_btn = get_node("Background/CloseBtn")

onready var res_mb = get_node("Background/VBoxContainer/ScrollContainer/VBoxContainer/ResCtn/MenuButton")
onready var wm_mb = get_node("Background/VBoxContainer/ScrollContainer/VBoxContainer/WMCtn/MenuButton")

onready var master_slider = get_node("Background/VBoxContainer/ScrollContainer/VBoxContainer/MasterCtn/Slider")
onready var bgm_slider = get_node("Background/VBoxContainer/ScrollContainer/VBoxContainer/BGMCtn/Slider")
onready var sfx_slider = get_node("Background/VBoxContainer/ScrollContainer/VBoxContainer/SFXCtn/Slider")
onready var ui_slider = get_node("Background/VBoxContainer/ScrollContainer/VBoxContainer/UICtn/Slider")

onready var asp = get_node("ASP")

func _ready():
	master_slider.value = ConfigHandler.sd_master
	bgm_slider.value = ConfigHandler.sd_bgm
	sfx_slider.value = ConfigHandler.sd_sfx
	ui_slider.value = ConfigHandler.sd_ui
	
	res_mb.selected = ConfigHandler.resolutions[ConfigHandler.resolution]
	wm_mb.selected = ConfigHandler.window_mode
	
	verify_wm(ConfigHandler.window_mode)
	
	close_btn.connect("pressed", self, "on_btn_pressed")
	res_mb.connect("pressed", self, "on_btn_pressed")
	wm_mb.connect("pressed", self, "on_btn_pressed")
	close_btn.connect("pressed", self, "on_close_pressed")
	res_mb.connect("item_selected", self, "on_resolution_change")
	wm_mb.connect("item_selected", self, "on_window_mode_change")
	master_slider.connect("value_changed", self, "on_master_value_updated")
	bgm_slider.connect("value_changed", self, "on_bgm_value_updated")
	sfx_slider.connect("value_changed", self, "on_sfx_value_updated")
	ui_slider.connect("value_changed", self, "on_ui_value_updated")

func on_btn_pressed():
	asp.play()

func on_close_pressed():
	queue_free()

func vm_button_handle(index):
	if index == 0:
		res_mb.disabled = false
	else:
		res_mb.disabled = true

func verify_wm(index):
	vm_button_handle(index)
	
	if index == 0:
		ConfigHandler.window_mode = index
		res_mb.selected = ConfigHandler.resolutions[ConfigHandler.resolution]
	elif index == 1:
		ConfigHandler.window_mode = index
		res_mb.selected = ConfigHandler.resolutions[ConfigHandler.resolution]
	else:
		pass

func on_resolution_change(index):
	print(res_mb.get_item_text(res_mb.selected))
	ConfigHandler.resolution = res_mb.get_item_text(res_mb.selected)

func on_window_mode_change(index):
	verify_wm(index)

func on_master_value_updated(new_value):
	ConfigHandler.sd_master = new_value
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), new_value)

func on_bgm_value_updated(new_value):
	ConfigHandler.sd_bgm = new_value
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("BGM"), new_value)

func on_sfx_value_updated(new_value):
	ConfigHandler.sd_sfx = new_value
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("SFX"), new_value)

func on_ui_value_updated(new_value):
	ConfigHandler.sd_ui = new_value
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("UI"), new_value)

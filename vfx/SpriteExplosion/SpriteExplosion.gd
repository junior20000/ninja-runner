extends Particles2D


func _ready():
	$Lifetime.connect("timeout", self, "_on_lifetime_reach")

func _on_lifetime_reach():
	queue_free()

func init(sprite: Sprite, parent_scale: Vector2):
	var p_texture = _extract_frame_texture(sprite, parent_scale)
	
	process_material.set_shader_param("emission_box_extends", Vector3(p_texture.get_width(), p_texture.get_height(), 1))
	process_material.set_shader_param("sprite", p_texture)
	
	amount = 2048
	
	emitting = true


func _extract_frame_texture(sprite: Sprite, parent_scale: Vector2) -> ImageTexture:
	var itex: ImageTexture = ImageTexture.new()
	var t_data: Image = sprite.texture.get_data()
	var t_size: Vector2 = sprite.texture.get_size()
	var frame_size: Vector2 = Vector2(t_size.x / sprite.hframes, t_size.y / sprite.vframes)
	var frame_pos: Vector2 = Vector2(frame_size.x * sprite.frame_coords.x, frame_size.y * sprite.frame_coords.y)
	
	var rect: Rect2 = Rect2(frame_pos, frame_size)
	
	t_data = t_data.get_rect(rect)
	t_data.resize(t_data.get_width() * sprite.scale.x * parent_scale.x, t_data.get_height() * sprite.scale.y * parent_scale.y, 0)  # FIX THIS!
	
	itex.create_from_image(t_data, 0)
	
	return itex
